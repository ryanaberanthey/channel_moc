from pylab import *
from scipy.io import loadmat

m = loadmat('../channel_model_config/model_data.mat')
z = m['mygrid']['Z'][0][0][:,0]
zf = m['mygrid']['Zfine'][0][0][0,:]
y = m['mygrid']['Y'][0][0][:,0] / 1000.

chanK = m['model_data'][0][14]
chanKl = m['model_data'][0][40]

clev = arange(-20,60.1)*100.
cblev = arange(-2,6.1)*1000.
tlev = arange(0,10,0.5)
Ulev = arange(0,15,2)
Ueddylev = arange(0,41,5)
PVlev = arange(-10,11)
Beta = 1e-11

fignum=1

names = dict(K='Standard', Kl30='No Sponge')

for my in [chanK, chanKl]:

    name = names[my['letter'][0]]
    U = my['UBar']
    QGPV_grad = my['QGPV_grad']
    (Nz,Ny) = U.shape
    UBc = U - tile(U[-1,:],(Nz,1))
    uEddy = (2*my['EKE'])**0.5 

    figure(fignum)
    contourf(y,z[1:], QGPV_grad/Beta, PVlev, extend='both', cmap=get_cmap('bwr'))
    cb=colorbar()
    contour(y,z,my['TBar'], tlev, colors='k')
    contour(y,z,UBc*100., Ulev, colors='m')
    title(r'QGPV grad ($\beta$) - %s'  % name)

    figure(fignum+1)
    contourf(y,z,uEddy*100, Ueddylev, extend='both')
    cb=colorbar()
    contour(y,z,my['TBar'], tlev, colors='k')
    contour(y,z,UBc*100., Ulev, colors='m')
    title(r'RMS Eddy Velocity (cm) - %s' % name)

    fignum+=2


