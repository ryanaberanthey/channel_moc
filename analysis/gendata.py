from pylab import *
from os.path import join
from scipy.ndimage.filters import gaussian_filter
#from MITgcm.utils.mds import rdmds
from MITgcmutils.mds import rdmds

# where to put the files
output_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V3/input'
# what binary format to use
tp = '>f8'
# where to find the fields for the initial conditions
Tfile='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/output/nosponge/tave/Ttave.0008985600'

# square domain is usually bad--don't fuck it up
Nz,Ny,Nx = 40,400,400

# inital bouyancy 
Ttave = rdmds(Tfile)
Tnew = transpose(tile(Ttave.mean(axis=2),(Nx,1,1)),[1,2,0])
Tnew[:,-1] = Tnew[:,-2]
# add random noise
#Tnew = Tnew + 1e-3 * gaussian_filter((np.random.random((Nz,Ny,Nx)) - 0.5), 2)
Tnew = Tnew + 1e-3 * (np.random.random((Nz,Ny,Nx)) - 0.5)

# write
Tnew.astype(tp).tofile(join(output_dir, 'THETA_init.bin'))

# surface restoring
Tmin,Tmax = 0.,8.0
Tsurf = tile(linspace(Tmin,Tmax,Ny),(Nx,1)).T
# write
Tsurf.astype(tp).tofile(join(output_dir, 'THETA_surf_0to8.bin'))
	
# wind 
tau0 = logspace(log2(0.0125), log2(0.8), 7, base=2)
for t in tau0:
	tau = t * tile( sin(pi* arange(Ny) / Ny), (Nx,1) ).T
	tau.astype(tp).tofile(
		join( output_dir, 'TAUX_%04d.bin' % int(round(t*1e4))))

# bathymetry stuff
depth = -2985.
sill = -2058.

# flat bathymetry
bath_flat = depth * ones((Ny,Nx))
bath_flat[0,:] = 0.
bath_flat.astype(tp).tofile(join(output_dir, 'BATHY_flat.bin'))

# bathymetry with sill
bathy = depth * ones((Ny,Nx))
bathy[:,0] = sill
bathy[0,:] = 0.
bathy.astype(tp).tofile(join(output_dir, 'BATHY_sill%04d.bin' % -sill))

# gaussian bump
x = arange(Nx, dtype=dtype('f8'))
bumpwidth = 10 # half width
bump = depth - (depth-sill) * exp( - (x - (Nx/2))**2  / (2*bumpwidth**2) )
bathy_bump = tile( bump, (Ny, 1) )
bathy_bump[0,:] = 0.
bathy_bump.astype(tp).tofile(join(output_dir, 'BATHY_bump%04d.bin' % -sill))
