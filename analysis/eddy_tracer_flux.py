# encoding: utf-8
from pylab import *
from MITgcmutils import mds
import channel
import mycolors
import os

base_dir = r"/Volumes/My Passport/Backups.backupdb/rpa’s Mac Pro/2013-07-25-110258/Macintosh HD/Users/rpa/Data/DATASTORE.RPA"
c = channel.Channel(
    output_dir=os.path.join(base_dir,'projects/channel/V2/output/reference/ptracers_yearly_reset'),
    grid_dir=os.path.join(base_dir,'projects/channel/V2/grid'),
    default_iter = 5529600)

ntr = 6

Nx,Ny,Nz = c.Nx, c.Ny, c.Nz

# load V, W, T bar
# put everything on the C Grid
T = c.rdmds('Ttave')
V = c.vgrid_to_cgrid(c.rdmds('vVeltave'))
W = c.wgrid_to_cgrid(c.rdmds('wVeltave'))
VT= c.vgrid_to_cgrid(c.rdmds('VTtave'))
WT= c.wgrid_to_cgrid(c.rdmds('WTtave'))

VpTp = ( c.vgrid_to_cgrid(VT) - c.vgrid_to_cgrid(V)*T ).mean(axis=2)
WpTp = ( c.wgrid_to_cgrid(WT) - c.wgrid_to_cgrid(W)*T ).mean(axis=2)


# mean zonal flow
Ubar = c.rdmds('uVeltave').mean(axis=2)
Ubc = Ubar - tile(Ubar[-1,:],(Nz,1))
Tbar = T.mean(axis=2)
Ty = c.ddy_cgrid_centered(Tbar)
Tz = c.ddz_cgrid_centered(Tbar)
S = - Ty / Tz

Kb = - VpTp / Ty

# all the zonally-averaged tracer gradients
dQdz = zeros((ntr,Nz,Ny))
dQdy = zeros((ntr,Nz,Ny))

# all the tracer fluxes (zon avg)
VpQp = zeros((ntr,Nz,Ny))
WpQp = zeros((ntr,Nz,Ny))

Q = c.rdmds('trac_mean',useMask=False)
VQ= c.rdmds('trac_v',useMask=False)
WQ= c.rdmds('trac_w',useMask=False)

# normalization factors for each tracer
normfac = array([2000e3, 3000., 1, 1, 1, 1])
# (turns out to be completely unneccesary)

for n in range(ntr):
    dQdy[n] = c.ddy_cgrid_centered(Q[n].mean(axis=2)) / normfac[n]
    dQdz[n] = c.ddz_cgrid_centered(Q[n].mean(axis=2)) / normfac[n]
    VpQp[n] = ( c.vgrid_to_cgrid(VQ[n]) - c.vgrid_to_cgrid(V)*Q[n] ).mean(axis=2) / normfac[n]
    WpQp[n] = ( c.wgrid_to_cgrid(WQ[n]) - c.wgrid_to_cgrid(W)*Q[n] ).mean(axis=2) / normfac[n]

Qflux_slope = VpQp/WpQp

# the tensor calculation
K = zeros((Nz,Ny,4))
res = zeros((Nz,Ny,2))
for k in arange(Nz):
    for j in arange(Ny):
        #print('%03d %03d' % (k,j) )
        grads = array( ( dQdy[:,k,j], dQdz[:,k,j] ) )
        fluxes = array( ( VpQp[:,k,j], WpQp[:,k,j] ) )
        Ktmp = lstsq( -grads.T, fluxes.T)
        if len(Ktmp[1]==2):
            res[k,j,:] = Ktmp[1]
        K[k,j,:] = Ktmp[0].flatten()
        
# re-estimate the tracers and calculate the errors
VpQp_est = zeros_like(VpQp)
WpQp_est = zeros_like(WpQp)
err_V = zeros_like(VpQp)
err_W = zeros_like(WpQp)
for n in range(ntr):
    VpQp_est[n] = - (K[:,:,0]*dQdy[n] + K[:,:,2]*dQdz[n])
    WpQp_est[n] = - (K[:,:,1]*dQdy[n] + K[:,:,3]*dQdz[n])
    err_V[n] = abs(VpQp[n] - VpQp_est[n])/abs(VpQp[n])
    err_W[n] = abs(WpQp[n] - WpQp_est[n])/abs(WpQp[n])
    # figure()
    # subplot(231); p=pcolormesh(c.yc, c.zc,VpQp[n]); title("V'Q'")
    # subplot(232); pcolormesh(c.yc, c.zc,VpQp_est[n]); title("V'Q' est"); clim(p.get_clim())
    # subplot(233); p=pcolormesh(c.yc, c.zc, ma.masked_invalid(err_V[n])); title("V error'"); clim([0,0.5])
    # subplot(234); p=pcolormesh(c.yc, c.zc,WpQp[n]); title("W'Q'")
    # subplot(235); pcolormesh(c.yc, c.zc,WpQp_est[n]); title("W'Q' est"); clim(p.get_clim())
    # subplot(236); p=pcolormesh(c.yc, c.zc, ma.masked_invalid(err_W[n])); title("W error'"); clim([0,0.5])

err_V = ma.masked_invalid(err_V)
err_W = ma.masked_invalid(err_W)
    
# buoyancy
VpTp_est = - (K[:,:,0]*Ty + K[:,:,2]*Tz)
WpTp_est = - (K[:,:,1]*Ty + K[:,:,3]*Tz)
err_Vb = ma.masked_invalid( abs(VpTp - VpTp_est)/abs(VpTp) )
err_Wb = ma.masked_invalid( abs(WpTp - WpTp_est)/abs(WpTp) )
figure()
subplot(221); p=pcolormesh(c.yc, c.zc,VpTp); title("V'T'")
subplot(222); pcolormesh(c.yc, c.zc,VpTp_est); title("V'T' est"); clim(p.get_clim())
subplot(223); p=pcolormesh(c.yc, c.zc,WpTp); title("W'T'")
subplot(224); pcolormesh(c.yc, c.zc,WpTp_est); title("W'T' est"); clim(p.get_clim())


Ks = zeros(shape(K)) # symmetric part
Ka = zeros(shape(K)) # antisym
Th = zeros((Nz,Ny))  # rotation angle of symmetric part
Dyy = zeros((Nz,Ny))   # major axis of sym part
Dzz = zeros((Nz,Ny))

for k in arange(Nz):
    for j in arange(Ny):
        D = K[k,j,:]
        D.shape = (2,2)
        ks = 0.5*(D + D.T)
        ka = 0.5*(D - D.T)
        Ks[k,j,:] = ks.flatten()
        Ka[k,j,:] = ka.flatten()
        th = arctan2( 2*ks[0,1], ks[0,0]-ks[1,1] ) / 2
        s = S[k,j]
        Th[k,j] = th
        Dyy[k,j] = ( cos(th)**2 * ks[0,0] + sin(th)**2 * ks[1,1]
            + 2*sin(th)*cos(th)*ks[0,1] )
        Dzz[k,j] = ( sin(th)**2 * ks[0,0] + cos(th)**2 * ks[1,1]
            - 2*sin(th)*cos(th)*ks[0,1] )



#Y = c.yc
Y = linspace(0,2000,400)
Z = c.zc    

Psi = Ka[:,:,1] * c.Lx

# buoyancy flux
Kbtens = ma.masked_array(-Ka[:,:,1]*Tz/Ty, Tz<1e-4)
Kblevs = arange(0,4001,200);

Tzmaskval = 2e-4
# QGPV
Qy = ma.masked_array( c.beta - c.f0 * c.ddz_cgrid_centered(-Ty/Tz),  Tz<Tzmaskval)
VpQp = ma.masked_array( c.f0 * c.ddz_cgrid_centered(VpTp / Tz),  Tz<Tzmaskval)
Kq = ma.masked_array(-VpQp/Qy, abs(Qy)<abs(c.beta/2) )

savez('data/tensor.npz', Psi=Psi, th=th, Dyy=Dyy, Dzz=Dzz, Y=Y, Z=Z, Tbar=Tbar.filled(0.))
savez('data/tensor_raw.npz', K=K, Y=Y, Z=Z, Tbar=Tbar.filled(0.))
savez('data/KqKb.npz', Kq=Kq.filled(nan), Kb=Kb.filled(nan), Y=Y, Z=Z, Tbar=Tbar.filled(nan))

# true eddy MOC for comparison
psi_iso_z = c.get_psi_iso_z()
psi_bar = c.get_psi_bar()
psi_eddy = psi_iso_z - psi_bar


bfac = 9.8*2e-4

close('all')
rc('figure.subplot', left=0.1, right=0.99, bottom=0.2, top=0.9, wspace=0.12)
rc('lines', linewidth=0.5)
rc('font', size=8)
rcParams['axes.formatter.limits']=[-4, 4]

figure(figsize=(6.5,2.4))
subplot(121)
pcolormesh(Y,Z,bfac* (c.ddy_vgrid_to_cgrid(VT.mean(axis=2)) + c.ddz_wgrid_to_cgrid(WT.mean(axis=2))),
    rasterized=True, cmap=get_cmap('posneg')); clim(array([-1,1])*4e-11)
# pcolormesh(Y,Z, c.ddy_cgrid_centered(VpTp) + c.ddz_cgrid_centered(WpTp),
#     rasterized=True, cmap=get_cmap('posneg')); clim(array([-1,1])*1e-7)
colorbar()
xlabel('x (km)'); ylabel('z (m)'); ztk = gca().get_yticks()
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='k')
title(r'$\nabla \cdot \overline{\mathbf{u} b}$')
subplot(122)
pcolormesh(Y,Z,bfac* (c.ddy_cgrid_centered(VpTp-VpTp_est) + c.ddz_cgrid_centered(WpTp-WpTp_est)),
    rasterized=True, cmap=get_cmap('posneg')); clim(array([-1,1])*4e-11)
colorbar()
xlabel('x (km)'); yticks(ztk,[])
title(r"$\nabla \cdot \overline{(\mathbf{u}' b')_{err}}$")
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='k')

savefig('figures/ub_error.pdf')

# figures
#Dyylevs=arange(0.5,6.1,0.5)*1e3
Dyylevs=arange(0,7001,500)
Dzzlevs=arange(0,10.1,0.5)*1e-4
#Dcmap = get_cmap('RdBu_r')
#Dcmap = get_cmap('sun')
Dcmap = get_cmap('jet')
Psilevs=arange(-3,3.1,0.5)*1e6

# big Dyy figure
figure(figsize=(6.5,4.4))
cf=contourf(Y,Z,Dyy, Dyylevs, cmap=Dcmap, extend='both')
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='w', linewidths=1)
contour(Y,Z,Ubc,arange(0,12)/100., colors='0.5', linewidths=1)
for k in arange(1,Nz,2):
    for j in arange(9,Ny,20):
        r = 40000
        #r = max([10*Dyy[k,j],10000])
        arrow(Y[j],Z[k], r*cos(Th[k,j])/1000, r*sin(Th[k,j]), color='k', linewidth=0.3)
#xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
ylabel('Z (m)');
title(r"$D'_{yy}$ (m$^2$s$^{-1}$)")
tight_layout()
colorbar(cf)
savefig('figures/Dyy.pdf')

# big Dzz figure
figure(figsize=(6.5,4.4))
cf=contourf(Y,Z,Dzz, Dzzlevs, cmap=Dcmap, extend='both')
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='w', linewidths=1)
contour(Y,Z,Ubc,arange(0,12)/100., colors='0.5', linewidths=1)
for k in arange(1,Nz,2):
    for j in arange(9,Ny,20):
        r = 40000
        #r = max([10*Dyy[k,j],10000])
        arrow(Y[j],Z[k], r*cos(Th[k,j])/1000, r*sin(Th[k,j]), color='k', linewidth=0.3)
#xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
ylabel('Z (m)');
title(r"$D'_{zz}$ (m$^2$s$^{-1}$)")
tight_layout()
colorbar(cf)
savefig('figures/Dzz.pdf')


rcParams['axes.formatter.limits']=[-2, 4]
rc('figure.subplot', left=0.08, right=0.96, bottom=0.2, top=0.85, wspace=0.12)
figure(figsize=(6.5,2.1))

clf()
subplot(131)
pcolormesh(Y, Z,Ks[:,:,3], rasterized=True, cmap=get_cmap('posneg'))
clim(array([-1,1])*5e-3); colorbar()
xlabel('x (km)'); ylabel('z (m)'); ztk = gca().get_yticks()
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='k')
title(r"$D_{zz}$ (m$^{2}$ s$^{-1}$)")

subplot(132)
pcolormesh(Y,Z,-Ks[:,:,2]**2/Ks[:,:,0], rasterized=True, cmap=get_cmap('posneg'));
clim(array([-1,1])*5e-3); colorbar()
xlabel('x (km)'); yticks(ztk,[])
title(r"$-D_{yz}^2/D_{yy}$ (m$^{2}$ s$^{-1}$)")
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='k')

subplot(133)
pcolormesh(Y,Z,Dzz, rasterized=True, cmap=get_cmap('jet'));
clim(array([0,1])*1e-3); colorbar()
xlabel('x (km)'); yticks(ztk,[])
title(r"$D'_{zz}$ (m$^{2}$ s$^{-1}$)")
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='k')

savefig('figures/Dzz_components.pdf')

figure(figsize=(6.5,2.4))
Sv=1e6
subplot(121)
contourf(Y,Z,Psi/Sv, Psilevs/Sv, cmap=get_cmap('posneg'), extend='both')
colorbar()
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='k')
title('$\chi L_x$ (Sv)')
xlabel('x (km)'); ylabel('z (m)'); ztk = gca().get_yticks()
subplot(122)
contourf(Y,Z,psi_eddy/Sv, Psilevs/Sv, cmap=get_cmap('posneg'), extend='both')
colorbar()
contour(Y,Z,Tbar,arange(-1,10,0.5), colors='k')
xlabel('x (km)'); yticks(ztk,[])
title(r"$\Psi^\ast$ (Sv)")

savefig('figures/chi_and_psi.pdf')

# errors
figure(figsize=(6.5,4.4))
subplot(221)
hist(err_V.compressed(), normed=True, bins=40, range=(0,1.), color='k')
xlabel(r"$\overline{v'c'}$ relative error")
ylabel('frequency (%)')
ylim([0,20])
subplot(222)
hist(err_W.compressed(), normed=True, bins=40, range=(0,1.), color='k')
xlabel(r"$\overline{w'c'}$ relative error")
ylabel('frequency (%)')
ylim([0,20])
subplot(223)
hist(err_Vb.compressed(), normed=True, bins=40, range=(0,1.), color='k')
xlabel(r"$\overline{v'b'}$ relative error")
ylabel('frequency (%)')
ylim([0,6])
subplot(224)
hist(err_Wb.compressed(), normed=True, bins=40, range=(0,1.), color='k')
xlabel(r"$\overline{w'b'}$ relative error")
ylabel('frequency (%)')
ylim([0,6])
tight_layout()
savefig('figures/error_histogram.pdf')


#rc('figure.subplot', left=0.2, right=0.98, bottom=0.08, top=0.92, wspace=0.2)
rc('lines', linewidth=0.5)
rc('font', size=7)
figure(figsize=(6.5,2.1))

clf()
subplot(131);
contourf(Y, Z, Kb, Kblevs, cmap=Dcmap, extend='both')
colorbar(ticks=arange(0,4001,1000))
xlabel('x (km)'); ylabel('z (m)'); ztk = gca().get_yticks()
contour(Y, Z, Tbar,arange(-1,10,0.5), colors='k')
title(r'$K_b$ (m$^2$s$^{-1}$)')

subplot(132);
contourf(Y, Z,  Kbtens, Kblevs, cmap=Dcmap, extend='both')
colorbar(ticks=arange(0,4001,1000))
xlabel('x (km)');
yticks(ztk, [])
contour(Y, Z, Tbar,arange(-1,10,0.5), colors='k')
title(r'$K_b$ (m$^2$s$^{-1}$)')

title(r'$\chi / s$ (m$^2$s$^{-1}$)')

subplot(133)
scatter(Kb, Kbtens, c=tile(c.zc[:,newaxis],[1,c.Ny]), marker='.', edgecolors='none', rasterized=True)
colorbar(ticks=arange(-3000,0,500))
title(r'$K_b$ vs. $\chi / s$')
xlabel(r'$K_b$ (m$^2$s$^{-1}$)')
xticks(arange(0,5001,1000),['0','','2000','','4000'])
yticks(arange(0,5001,1000),[])
xlim([0,5000]); ylim([0,5000])
gcf().text(0.9, 0.87, 'depth (m)')
savefig('figures/Kb_scatter.pdf')



rc('figure.subplot', left=0.08, right=0.99, bottom=0.2, top=0.85, wspace=0.12)
figure(figsize=(6.5,2.1))

clf()
subplot(131, axisbg='0.8')
contourf(Y, Z, Qy / c.beta, arange(-10,10,2)+1, cmap=get_cmap('posneg'), extend='both')
xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
ylabel('Z (m)');
yticks(arange(-2500,0,500))
colorbar()
title(r"$Q_y$ (10$^{-11}$ m$^{-1}$s$^{-1}$)")

subplot(132, axisbg='0.8')
contourf(Y, Z, VpQp / 1e-7, arange(-5,5,1)+0.5, cmap=get_cmap('posneg'), extend='both')
xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
yticks(arange(-2500,0,500),[])
yticks(arange(-2500,0,500))
colorbar()
title(r"$v'q'$ (10$^{-7}$ m s$^{-2}$)")

subplot(133, axisbg='0.8')
contourf(Y, Z, Kq, Dyylevs, cmap=Dcmap, extend='both')
xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
yticks(arange(-2500,0,500),[])
yticks(arange(-2500,0,500))
colorbar()
title(r"$K_q$ (m$^2$s$^{-1}$)")

savefig('figures/Kq.pdf')

# a few comparisons
jr = r_[180:220] # averaging window
figure(figsize=(6.0,2.1))
subplot(121)
plot(VpQp[:,jr].mean(axis=1) / 1e-7, c.zc, 'k-')
plot(-Dyy[:,jr].mean(axis=1) * Qy[:,jr].mean(axis=1) / 1e-7, c.zc, 'k.-')
xlim(array([-2,2]))
grid()
legend([r"$\bar{v'q'}$", r"$-D'_{yy} Q_y$"], loc='lower left')
xlabel(r'(10$^{-7}$ m s$^{-2}$)')
ylabel('depth (m)')
title('Eddy QGPV Flux')

s = -(Ty/Tz)[:,jr].mean(axis=1)
kbs = 4-cumsum((Dyy[:,jr].mean(axis=1)*(c.beta/c.f0 - c.ddz_cgrid_centered(s))*c.dzf)[::-1])[::-1]
subplot(122)
plot(bfac*VpTp[:,jr].mean(axis=1) / 1e-5, c.zc, 'k-')
plot(bfac*kbs*Tz[:,jr].mean(axis=1) / 1e-5, c.zc, 'k.-')
xlabel(r'(10$^{-5}$ m$^2$ s$^{-3}$)')
ylabel('depth (m)')
xlim(array([-1.5,0.2]))
legend([r"$\bar{v'b'}$", r"$-\bar b_y [\Psi_0 + \int D'_{yy}(\beta/f_0 - s_z)dz]$"], loc='lower left')
title('Eddy Buoyancy Flux')
grid()
tight_layout()
savefig('figures/VpQpVpBp.pdf')


# final figure
psi_eddy = c.get_psi_bar() - c.get_psi_iso_z()
vstar = -c.ddz_cgrid_centered(psi_eddy / c.Lx)

figure(figsize=(2.6,2.1))
clf()
lhs = c.ddz_cgrid_centered( Kb[:,jr].mean(axis=1) * s)
rhs1 = c.ddz_cgrid_centered( Dyy[:,jr].mean(axis=1) * s)
rhs2 = Dyy[:,jr].mean(axis=1) * c.ddz_cgrid_centered(s)
rhs3 = Dyy[:,jr].mean(axis=1) * (-c.beta / c.f0 + c.ddz_cgrid_centered(s))
plot(lhs * 100., c.zc, 'k-', linewidth=1.5)
plot(rhs1 * 100., c.zc, 'k:', linewidth=1.)
plot(rhs2 * 100., c.zc, 'k--', linewidth=1.)
#plot(-rhs3 * 100., c.zc, 'k*', linewidth=1.)
plot( 100*vstar[:,jr].mean(axis=1), c.zc, 'r-', linewidth=1.)
ylabel('depth (m)')
legend([ r'$\partial_z ( K_b s)$', r'$\partial_z ( D_{yy} s)$', r'$D_{yy} \partial_z s$',
        r'$D_{yy} (\partial_z s - \beta/f)$'], loc='lower right')
xlim(array([-2.5,4])*1e-1)
xlabel('(cm s$^{-1}$)')
title(r'Relation btw. $D_{yy}$ and $K_b$')
#grid()
tight_layout()
savefig('figures/KqKb_relation.pdf')


