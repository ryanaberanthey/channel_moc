from pylab import *
from scipy.io import loadmat

m = loadmat('../channel_model_config/model_data.mat')
z = m['mygrid']['Z'][0][0][:,0]
zf = m['mygrid']['Zfine'][0][0][0,:]
y = m['mygrid']['Y'][0][0][:,0] / 1000.
my = m['model_data'][0][14]


clev = arange(-20,60.1)*100.
cblev = arange(-2,6.1)*1000.
tlev = arange(0,10,0.5)
Ulev = arange(0,15,2)
Ueddylev = arange(0,51,5)

U = my['UBar']
(Nz,Ny) = U.shape
UBc = U - tile(U[-1,:],(Nz,1))
uEddy = (2*my['EKE'])**0.5 

contourf(y,zf,my['K_QGPV'], clev, extend='both')
#contourf(y,z,my['K_T'], clev, extend='both')
cb=colorbar( ticks=cblev )
contour(y,z,my['TBar'], tlev, colors='w')
contour(y,z,UBc*100., Ulev, colors='m')
contour(y,z,uEddy*100, Ueddylev, colors='0.2')

savefig('figures/K_QGPV.png')



