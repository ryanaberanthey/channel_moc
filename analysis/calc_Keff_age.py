import channel
from effdiff import effdiff
from pylab import *

WN=3 # wind stress index

base_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/'
c = channel.Channel(base_dir + 'output/age/age_0.%1d' % WN, base_dir + 'grid_30')
lc = c.get_layers_computer()

# variables needed for Keff calculation
rac = ma.masked_array(c.rac, c.mask[0])
dx = tile(c.dxc,[c.Ny,1])
dy = tile(c.dyc,[c.Nx,1]).T

N = 100 # resolution of Keff calculation
Nt = 12

Yk = linspace(0,c.Ly,N)

iter0 = 5080320
dT = 2880

Ntot = 9*12  # 9 years of output
Nt = 24      # resetting every 10 years

Le2_g = zeros((Ntot, lc.Ng, N))
tr_mean_g = zeros((Ntot,lc.Ng, c.Ny))

# equivalent latitudes
Y = zeros(N)
Y_g = zeros_like(Le2_g)

for n in arange(1,Ntot):
    iter = iter0 + dT*n
    tr = c.rdmds('PTRACER01', iter)
    t = 0.5*(c.rdmds('THETA', iter-(dT/2)) + c.rdmds('THETA', iter+(dT/2)))
    # isopycnal Le2
    tr_t, h = lc.interp_to_g(tr,t)
    tr_mean_g[n] = tr_t.mean(axis=2)
    for kg in arange(lc.Ng):
        rac_g = ma.masked_array(rac, tr_t[kg].mask)
        if isfinite(rac_g.sum()):
            eng_g = effdiff.EffDiffEngine(rac_g,dx,dy,N)
            print(eng_g.tot_area / rac.sum())
            Y_g[n,kg] = (c.Ly-eng_g.A[-1]/c.Lx) + (eng_g.A/c.Lx)
            r = eng_g.calc_Le2(tr_t[kg])
            Le2_g[n,kg] = r['Le2']
        else:
            print('No area at this isopycnal, skipping.')

savez('data_age/Le2_monthly_age_0.%1d.npz' % WN, tr_mean_g=tr_mean_g, Le2_g=Le2_g, Y_g=Y_g)


