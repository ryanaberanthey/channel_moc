from pylab import *
from MITgcmutils.mds import rdmds
# do a netcdf file
from evtk.hl import gridToVTK 
#data_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V3/output/spinup/'
data_dir='/Volumes/scratch/tmp/THETA/'
output_dir='/Volumes/scratch/tmp/output_for_paraview/theta_channel_spinup/'

#iters = arange(288,94752 + 1, 576)
iters = arange(2976, 31392 + 1, 192)
N = len(iters)


xg = arange(0,401)*5.0
yg = arange(0,401)*5.0
zf = rdmds('/Users/rpa/Data/DATASTORE.RPA/projects/channel/V3/grid/RF').squeeze()

xc = arange(0,400)*5.0 + 2.5
yc = arange(0,400)*5.0 + 2.5
zc = rdmds('/Users/rpa/Data/DATASTORE.RPA/projects/channel/V3/grid/RC').squeeze()

zscale = 0.5 * 2000 / 3e3


for n in arange(N):
    th = rdmds(data_dir+'THETA',iters[n])

    #gridToVTK(output_dir+'THETA.%05d'%n, xc, yc, zf*zscale, cellData= {"theta": transpose(th)})
    gridToVTK(output_dir+'THETA_pts.%05d'%n, xc, yc, zc*zscale, pointData= {"theta": transpose(th)})
