from pylab import *
import channel
import mycolors
from os import path

# dictionary of runs and the iteration number
runs = dict(
        reference = 5529600,
        linear = 6220800,
        nosponge = 8985600,
    )
    
base_dir = '/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2'
grid_dir = path.join(base_dir, 'grid')

Sv = 1e6
km = 1e3
Q_levs = (arange(-10,10)+0.5)
Q_ticks = arange(-8,9,2.)
Psi_levs = Q_levs / 10
Psi_ticks = Q_ticks / 10
T_levs = arange(0,9,0.5)
N2_levs = arange(0,16) 

labs = []

close('all')
for r in runs.keys():
    c = channel.ChannelSetup(path.join(base_dir,'output',r,'tave'), grid_dir,
            default_iter=runs[r])

    y = c.yc/km; z = c.zc     
    
    figure(1)
    plot( c.get_zonal_avg('Ttave')[:,-10],z)
    
    figure()
    cf=contourf(y, z, c.get_psi_iso_z()/Sv, Psi_levs, cmap=get_cmap('posneg'), extend='both')
    xlabel('Y (km)'); ylabel('Z (m)')
    contour(y,z, c.get_zonal_avg('Ttave'), colors='k')
    title(r'$\Psi_{iso}$ (Sv) - ' + r)
    tight_layout()
    colorbar(cf, ticks=Psi_ticks, shrink=0.8)
    savefig('figures/psi_iso_z-%s.png' % r)
    
    figure()
    cf=contourf(y, z, c.get_N2()*1e6, N2_levs, cmap=get_cmap('sun'), extend='both')
    xlabel('Y (km)'); ylabel('Z (m)')
    contour(y,z, c.get_zonal_avg('Ttave'), colors='k')
    title(r'$N^2$ (10$^{-6}$ s$^{-2}$) - ' + r)
    tight_layout()
    colorbar(cf, shrink=0.8)
    savefig('figures/N2-%s.png' % r)
    
    
    figure()
    cf=contourf(y, z, c.get_qgpv_grad()/c.beta, Q_levs, cmap=get_cmap('posneg'), extend='both')
    xlabel('Y (km)'); ylabel('Z (m)')
    contour(y,z, c.get_zonal_avg('Ttave'), colors='k')
    title(r'$\partial Q / \partial y$ ($\beta$) - ' + r)
    tight_layout()
    colorbar(cf, ticks=Q_ticks, shrink=0.8)    
    savefig('figures/qgpv_grad-%s.png' % r)

figure(1)
legend(runs.keys())

show()