from pylab import *
from scipy.io import loadmat
import channel
d100 = loadmat('data/K_40l_02_100d.mat', squeeze_me=True)
d150 = loadmat('data/K_40l_02_150d.mat', squeeze_me=True)
t = np.load('data/tensor.npz')

c = channel.Channel(
    output_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/output/reference/ptracers_yearly_reset',
    grid_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/grid',
    default_iter = 5529600)

lc = c.get_layers_computer()

Z = t['Z']
Tbar = t['Tbar']

# Andreas lagrangian data
ad = np.load('data/D_lagrange.npz')

# Nakamura effective diffusivity
nak = np.load('data/Keff_mean.npz')
G = nak['G']
Keff = ma.masked_array(nak['Keff_iso'], nak['mask'])
Yk = nak['Y']

jr = r_[50:350]

# Kp
kpfile = np.load('data/Kp.npz')
Kp = ma.masked_invalid(kpfile['Kp'])
Kp.mask = Kp.mask | (kpfile['PI']<0.98) | (Kp<0.)
hbar = kpfile['hbar']
G = kpfile['G']
Zg = interp(G, Tbar[::-1,200], Z[::-1])
Kpmean = (Kp[:,jr] * hbar[:,jr]).sum(axis=1) / hbar[:,jr].sum(axis=1)


# maks levels without enough points
vmask = ((~Kp.mask).mean(axis=1) < (kpfile['PI']>0.98).mean(axis=1)/2) | (Zg==-5)

# Kb and Kq
kqkb = np.load('data/KqKb.npz')
Kb = ma.masked_invalid(kqkb['Kb'])
Kb.mask += Kb < 10
#Kb.mask += kpfile['PIz'] < 0.95
Kq = ma.masked_invalid(kqkb['Kq'])
Kq.mask += Kq < 10 

# for some reason this is not correct in tensor.npz
Y = 5000.*arange(0,400) + 2500

# comparison in the horizontal plane
h = c.rdmds('../tave/layers_HV-tave', 5529600, useMask=False).mean(axis=2)
Keff_i = empty((lc.Ng,c.Ny))
newmask = empty((lc.Ng,c.Ny))
for k in arange(lc.Ng):
    Keff_i[k] = interp(c.yc,Yk,Keff[k])
    newmask[k] = interp(c.yc,Yk,Keff[k].mask)
Keff_i = ma.masked_array(Keff_i, newmask>0)
Keff_z = lc.transform_g_to_z(Keff_i.filled(nan),h)
Kp_z = lc.transform_g_to_z(Kp,h)
Keff_z = ma.masked_invalid(Keff_z)
Kp_z = ma.masked_invalid(Kp_z)
Kp_z.mask += (Kp_z < 1.)

close('all')

rc('figure.subplot', left=0.08, right=0.89, bottom=0.2, top=0.85, wspace=0.12)
rc('font', size=7)

figure(figsize=(6.5,2.1))
clf()

myticks = arange(0,8001,2000)
subplot(131)
scatter(t['Dyy'], Keff_z, c=tile(c.zc[:,newaxis],[1,c.Ny]), marker='.', edgecolors='none', rasterized=True)
xlim([0,8000]); ylim([0,8000])
title(r"$D'_{yy}$ vs. $K_{eff}^{iso}$")
xlabel(r"$D'_{yy}$ (m$^{2}$ s$^{-1}$)")
xticks(myticks); yticks(myticks)
subplot(132)
scatter(t['Dyy'], ma.masked_invalid(Kq), c=tile(c.zc[:,newaxis],[1,c.Ny]), marker='.', edgecolors='none', rasterized=True)
xlim([0,8000]); ylim([0,8000])
title(r"$D'_{yy}$ vs. $K_q$")
xlabel(r"$D'_{yy}$ (m$^{2}$ s$^{-1}$)")
xticks(myticks); yticks(myticks,[])
subplot(133)
scatter(t['Dyy'], Kp_z, c=tile(c.zc[:,newaxis],[1,c.Ny]), marker='.', edgecolors='none', rasterized=True)
xlim([0,8000]); ylim([0,8000])
xlabel(r"$D'_{yy}$ (m$^{2}$ s$^{-1}$)")
title(r"$D'_{yy}$ vs. $K_P$")
xticks(myticks); yticks(myticks,[])
cb=colorbar(cax=axes([0.91,0.2,0.01,0.65]), ticks=arange(-3000,0,500))
gcf().text(0.9, 0.89, 'depth (m)')
savefig('figures/K_scatter.pdf')

# surface plot
rc('lines', linewidth=0.5)
rc('font', size=6)
rcParams['legend.fontsize'] = 6

figure(figsize=(3.0,2.25))
plot(Y/1000., t['Dyy'][5], 'k-')
plot(nak['Y']/1000.,nak['Keff_z'][5], 'k--')
plot(Y/1000., kqkb['Kb'][5], 'k-.')
plot(1000., ad['D_yy'][1], 'k*', markersize=5)
xlim([0,2000])
xlabel('Y (km)')
ylabel(r'm$^2$ s$^{-1}$')
title('Surface Diffusivity Comparison')
legend([r'$D_{yy}$', r'$K_{eff}$', r'$K_b$', 'Lagrangian'], loc='upper left')
tight_layout()
savefig('figures/K_compare_surface.pdf')


# mask SDL
Kq.mask += kpfile['PIz'] < 0.95
Dyy = ma.masked_array(t['Dyy'], kpfile['PIz'] < 0.95)
Keff.mask += (kpfile['PI'][:,::4]<0.98)
Keff_mean = (Keff * hbar[:,::4]).sum(axis=1) / hbar[:,::4].sum(axis=1)


# different half-widths
widths = [10,100,150,200]
cols = ['k','b','g','r','y']
lstys = ['k:', 'k-.', 'k--', 'k-']
Nwidths = len(widths)
dy=5 # km

leg=[]

rc('figure.subplot', left=0.1, right=0.95, bottom=0.17, top=0.88, wspace=0.12)
figure(figsize=(6.5,2.25))

subplot(121)
for i in arange(Nwidths):
    jr=r_[(200-widths[i]):(200+widths[i])]
    Dyy_avg = Dyy.data[:,jr].mean(axis=1)
    plot(Dyy_avg,Z, lstys[i])
    leg.append('$\Delta$y=%g km' % (2*widths[i]*dy))
legend(leg, loc='lower left')
#grid()
xlabel(r'$\langle D_{yy} \rangle$ (m$^2$ s$^{-1}$)')
ylabel('Depth (m)')
xlim([0,6000])
title('Horizontal Averaging')

Ny=400
# average in isoyopycnal space
ZZ = tile(Z,(Ny,1)).T
dT = 0.2
T0 = arange(0,4.,dT)
ZT0 = interp(T0, Tbar[::-1,200], Z[::-1])

Dyy_T0 = empty((Nwidths,len(T0)))

Z0 = empty_like(Dyy_T0)
Kb_T0 = empty_like(Z0)
Kq_T0 = empty_like(Z0)
Kb_Z = empty((Nwidths,len(Kb)))

dzf = tile(c.dzf[:,newaxis],[1,Ny])
subplot(122)
for i in arange(Nwidths):
    jr=r_[(200-widths[i]):(200+widths[i])]
    for n in arange(len(T0)):
        Tmin = T0[n] - dT/2
        Tmax = T0[n] + dT/2
        idx = (Tbar[:,jr] >= Tmin) & (Tbar[:,jr] < Tmax)
        mydzf = dzf[:,jr][idx]
        totArea = mydzf.sum()
        Z0[i,n] = (ZZ[:,jr][idx] * mydzf).sum() / totArea
        Dyy_T0[i,n] = (Dyy[:,jr][idx] * mydzf).sum() / totArea
        Kb_T0[i,n] = (Kb[:,jr][idx] * mydzf).sum() / totArea
        Kq_T0[i,n] = (Kq[:,jr][idx] * mydzf).sum() / totArea
        #Dyy_T0[i,n] = ((Dyy[:,jr][idx]**-1 * mydzf).sum() / totArea)**-1
        #Kb_T0[i,n] = ((Kb[:,jr][idx]**-1 * mydzf).sum() / totArea)**-1
        #Kq_T0[i,n] = ((Kq[:,jr][idx]**-1 * mydzf).sum() / totArea)**-1
        
        Kidx = (G >= Tmin) & (G < Tmax)
        Yidx = (Yk >= Y[jr[0]] ) & (Yk < Y[jr[-1]])
    
    Kb_Z[i,:] = Kb[:,jr].mean(axis=1)
    plot(Dyy_T0[i],ZT0, lstys[i])  
    
legend(leg, loc='lower left')
#grid()
xlabel(r'$\langle D_{yy} \rangle$ (m$^2$ s$^{-1}$)')
#ylabel('Depth (m)')
xlim([0,6000])
locs,labels = yticks()
yticks(locs, [])
title('Isopycnal Averaging (Interior Only)')
savefig('figures/Dyy_deltaY_compare.pdf')

#### master figure
Zdia = interp(0.95, kpfile['PIz'].mean(axis=1), Z) # avg depth of diabatic layer

myi = 2 # which averaging width to use

close('all')
rc('figure.subplot', left=0.13, right=0.95, bottom=0.11, top=0.93)
figure(figsize=(6.5, 4.))

myleg = [r'$\langle D_{yy} \rangle$',r'$\langle K_{eff} \rangle$',
        r'$\langle K_P \rangle$',r'$\langle K_b \rangle$','Lagrangian','Tracer']

clf()
# black and white
plot(Dyy_T0[myi], Z0[myi], 'k-', linewidth=2)
plot(Keff_mean[:20], Zg[:20],'k--', linewidth=2)
plot(Kpmean[~vmask], Zg[~vmask], 'k*-', markersize=5)
#plot(Kb_T0[0], ZT0, 'k-.', linewidth=2)
plot(Kb_Z[myi], Z, 'k-.', linewidth=2)
plot(ad['D_yy'], ad['z'], 'ko-', markersize=4)
plot(d100['KTtarg'], d100['ZTtarg'], 'k^-', markersize=5)

gca().add_patch(Rectangle([0,Zdia],6000,-Zdia, color='0.8'))

legend(myleg, loc='lower left')
#grid()
xlabel(r'Diffusivity (m$^2$ s$^{-1}$)')
ylabel('Depth (m)')
title('Diffusivity Estimate Comparison')

savefig('figures/K_compare_bw.pdf')

# color
