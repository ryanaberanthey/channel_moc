from pylab import *
import channel
import mds

data_root = '/Users/rpa/Data/DATASTORE.RPA/'
data_dir = data_root + 'projects/channel/V2/output/0003456000'
grid_dir = data_root + 'projects/channel/V2/grid'

c = channel.ChannelSetup(data_dir, grid_dir)

Nx,Ny,Nz = c.Nx, c.Ny, c.Nz
Lx,Ly,H = c.Lx, c.Ly, c.H

y,z = meshgrid(c.yc, c.zc)

tracers = [ y, z,
    cos(pi*y/Ly) * cos(pi*z/H), sin(pi*y/Ly) * sin(pi*z/H),
    sin(pi*y/Ly) * sin(2 *pi*z/H), cos(2*pi*y/Ly) * cos(pi*z/H) ]


# write tracers
tp = '>f8'
for n in range(len(tracers)):
    fname = 'tracers/tr%02d.bin' % (n+1)
    transpose( tile(tracers[n], (Nx,1,1)), (1,2,0)
             ).astype(tp).tofile(fname)




