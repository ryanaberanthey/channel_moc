from pylab import *
from MITgcmdata import MITgcmmodel
import re
import os

class Channel(MITgcmmodel.ModelInstance):
    """Subclass of MITgcmdata.ModelSetup specified for the zonal channel"""

    def __init__(self, output_dir, grid_dir, default_iter=0,
                f0=-1e-4, beta=1e-11, g=9.8, tAlpha=2e-4, rho0=999.8):
        
        self.f0 = f0
        self.beta = beta
        self.g = g
        self.tAlpha = tAlpha
        self.rho0 = rho0
        self.ouput_dir = output_dir
        self.grid_dir = grid_dir
        self.default_iter = default_iter
            
        MITgcmmodel.ModelInstance.__init__(self,
                output_dir=output_dir, grid_dir=grid_dir, default_iter=self.default_iter)
    
    def calc_EKE(self):
        return 0.5 * ((self.rdmds('UUtave') - self.rdmds('Uveltave')**2) 
                    + (self.rdmds('VVtave') - self.rdmds('Vveltave')**2))
    
    def get_psi_iso(self):
        vflux = self.rdmds('layers_VFlux-tave', useMask=False)
        return -cumsum(vflux.mean(axis=2), axis=0) * self.Lx
    
    def get_psi_iso_z(self):
        psi_iso = self.get_psi_iso()
        hv = self.rdmds('layers_HV-tave', useMask=False)
        lc = self.get_layers_computer()
        return lc.transform_g_to_z(psi_iso, hv.mean(axis=2))

    def get_psi_bar(self, V=None, zpoint='C'):
        if V is None:
            V = self.rdmds('vVeltave')
        vflux = V * self.dzf[:,newaxis,newaxis] * self.hFacS
        psi = cumsum(vflux.mean(axis=2), axis=0) * self.Lx
        if zpoint=='F':
            return psi
        elif zpoint=='C':
            psi = vstack([zeros(self.Ny), psi])
            return 0.5 * (psi[1:] + psi[:-1])
        


