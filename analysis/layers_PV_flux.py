from pylab import *
from MITgcmutils import mds
import channel
import mycolors

c = channel.Channel(
    output_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/output/reference/ptracers_yearly_reset',
    grid_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/grid',
    default_iter = 5529600)
    
VH = c.rdmds('layers_VH-tave', useMask=False)
V = c.rdmds('layers_V-tave', useMask=False)
Hs = c.rdmds('layers_Hs-tave', useMask=False)
PIs = c.rdmds('layers_PIs-tave', useMask=False)

PImask = PIs.mean(axis=2) < 0.05

fgrid = c.f0 + tile(c.beta*(c.yc-c.yc.mean()),[V.shape[0],1])

hbar = Hs.mean(axis=2)

# Ertel PV
P = ma.masked_array(fgrid*PIs.mean(axis=2)/hbar, PImask)
dPdy = 0.5 * ( P[:,r_[1:c.Ny,c.Ny-1]] - P[:,r_[0,0:c.Ny-1]] ) / c.dyc[0]
dPdy.mask = dPdy.mask | (abs(hbar*dPdy) < 0.5e-11) 

# Thickmess weighted Mean V
Vstar = ma.masked_array(VH.mean(axis=2)/hbar, PImask)

# total flux of PV
VPstar = ma.masked_array(fgrid*(PIs*V).mean(axis=2)/hbar, PImask)

# eddy flux of PV
VPeddy = (VPstar - Vstar*P)

# diffusivity
Kp = -VPeddy / dPdy

close('all')
rc('lines', linewidth=0.5)
rc('font', size=6)
rc('figure.subplot', left=0.08, right=0.99, bottom=0.2, top=0.85, wspace=0.12)
figure(figsize=(6.5,2.1))

Y = c.yc
Z = c.zc
lc = c.get_layers_computer()
G = lc.G.squeeze()
Dyylevs=arange(0,7001,500)

savez('data/Kp.npz', Kp=Kp.filled(nan), PI=PIs.mean(axis=2), Y=c.yc,  G=lc.G.squeeze(),
                    PIz=lc.transform_g_to_z(PIs.mean(axis=2), hbar), hbar=hbar)


clf()
subplot(131, axisbg='0.8')
#contourf(Y, G, dPdy / 1e-13, arange(-16,16,2)+1, cmap=get_cmap('posneg'), extend='both')
contourf(Y, G, hbar * dPdy / 1e-11, arange(-10,10,2)+1, cmap=get_cmap('posneg'), extend='both')
colorbar()
contour(Y,G,PIs.mean(axis=2), [0.05, 0.5,.95],colors='k')
xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
yticks(range(8))
ylabel(r'T ($^\circ$C)');
#title(r"$P_y$ (10$^{-13}$ m$^{-2}$s$^{-1}$)")
title(r"$\bar \rho_b ^\ast \bar P^\ast_y$ (10$^{-11}$ m$^{-2}$s$^{-1}$)")

subplot(132, axisbg='0.8')
#contourf(Y, G, VPeddy / 1e-9, arange(-5,5,1)+0.5, cmap=get_cmap('posneg'), extend='both')
contourf(Y, G, hbar * VPeddy / 1e-7, arange(-5,5,1)+0.5, cmap=get_cmap('posneg'), extend='both')
colorbar()
contour(Y,G,PIs.mean(axis=2), [0.05, 0.5,.95],colors='k')
xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
yticks(range(8),[])
#title(r"$\hat v \hat q$ (10$^{-9}$ s$^{-2}$)")
title(r"$\bar \rho_b ^\ast \hat v \hat q$ (10$^{-7}$ s$^{-2}$)")

subplot(133, axisbg='0.8')
contourf(Y, G, Kp, Dyylevs, cmap=get_cmap('jet'), extend='both')
colorbar()
contour(Y,G,PIs.mean(axis=2), [0.05, 0.5,.95],colors='k')
xticks(arange(5)*5e5, range(0,2100,500))
xlabel('Y (km)');
yticks(range(8),[])
title(r"$K_P$ (m$^2$s$^{-1}$)")

savefig('figures/Kp.pdf')



