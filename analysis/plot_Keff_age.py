from pylab import *
import channel

base_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/'
c = channel.Channel(base_dir + 'output/reference/ptracers_Keff', base_dir + 'grid')

d_all = { 'tau_01': np.load('data_age/Le2_monthly_age_0.1.npz'),
      'tau_02': np.load('data/Le2_monthly.npz'),
      'tau_03': np.load('data_age/Le2_monthly_age_0.3.npz') }
myr = ['tau_01', 'tau_02', 'tau_03']

Klim = (0,7000)
Klevs = arange(0,8001,500)

close('all')
rc('figure.subplot', left=0.08, right=0.92, bottom=0.2, top=0.85, wspace=0.25)
rc('lines', linewidth=0.5)
rc('font', size=6)

Keff_mean = dict.fromkeys(d_all)
Keff_year = dict.fromkeys(d_all)
lc = c.get_layers_computer()

Ng = 42
Ntot = 108
#Nt = 108
#Nt = 12
Nt = 24
N = 100 # Keff levels
k0 = 56

yc = c.yc
Y = d_all['tau_02']['Y']


for r in d_all.keys():
#for r in ['tau_01']:

    Le2_g = zeros((Nt,Ng,N))
    Le2_g_count = zeros_like(Le2_g)

    d = d_all[r]

    for n in arange(Ntot):
        ni = n % Nt
        myLe2g = d['Le2_g']
        Y_g = d['Y_g']
        for k in arange(Ng):
            Le2_gi = interp(Y, Y_g[n,k], myLe2g[n,k], right=nan, left=nan)
            Le2_g[ni,k] += ma.masked_array(Le2_gi,isnan(Le2_gi))
            Le2_g_count[ni,k] += ~isnan(Le2_gi)
            
        if False:
            figure(1, figsize=(4,3))
            clf()
            contourf(Y/1000.,squeeze(lc.G), k0*ma.masked_invalid(Le2_g[ni])/c.Lx**2, Klevs, extend='max')
            ylim([0,8])
            title(r'$K_{eff}^{iso}$ (m$^2$s$^{-1}$) - %s | %g' % (r,n))
            xlabel('Y (km)'); ylabel('T ($^\circ$C)')
            colorbar(cax=axes([0.93,0.2,0.01,0.65]))
            show()
            pause(0.5)


    Le2_g = ma.masked_array(Le2_g / Le2_g_count, Le2_g_count==0)



    # interpolate isopycnal to height
    # h = c.rdmds('../tave/layers_HV-tave', 5529600, useMask=False).mean(axis=2)
    # hi = empty((Ng,N))
    # Le2_gz = empty_like(Le2)
    # for k in arange(Ng):
    #     hi[k] = interp(Y,c.yc,h[k])
    # lc = c.get_layers_computer()
    # for n in arange(Nt):
    #     Le2_gz[n] = lc.transform_g_to_z(Le2_g[n],hi)

    Le2_g_mean = Le2_g[6:12].mean(axis=0)
    #Le2_gz_mean = lc.transform_g_to_z(Le2_g_mean, hi)
    #Le2_gz_mean = ma.masked_equal(Le2_gz_mean,0)
    #Le2_mean = Le2[6:12].mean(axis=0)

    #Le2 = ma.masked_invalid(Le2)
    Le2_g = ma.masked_invalid(Le2_g)
    #Le2_gz = ma.masked_invalid(Le2_gz)
    
    Keff_iso = k0*Le2_g_mean/c.Lx**2
    np.savez('data_age/Keff_mean_%s.npz' % r, G=lc.G.squeeze(), Keff_iso=Keff_iso.filled(0.),
                                mask=Le2_g_mean.mask, Y=Y)
    Keff_mean[r] = Keff_iso
    Keff_year[r] = k0*Le2_g[11]/c.Lx**2



figure(2, figsize=(6.5,2.1))
clf()
for n in arange(3):
    r = myr[n]
    subplot(1,3,n+1)
    contourf(Y/1000.,squeeze(lc.G), Keff_mean[r], Klevs, extend='max')
    ylim([0,8])
    title(r'$K_{eff}^{iso}$ (m$^2$s$^{-1}$) - %s' % r)
    xlabel('Y (km)'); ylabel('T ($^\circ$C)')
colorbar(cax=axes([0.93,0.2,0.01,0.65]))
savefig('figures_age/Keff.pdf')
    
figure(3, figsize=(6.5,2.1))
clf()
for n in arange(3):
    r = myr[n]
    subplot(1,3,n+1)
    contourf(Y/1000.,squeeze(lc.G), Keff_year[r], Klevs, extend='max')
    ylim([0,8])
    title(r'$K_{eff}^{iso}$ (m$^2$s$^{-1}$) - %s' % r)
    xlabel('Y (km)'); ylabel('T ($^\circ$C)')
colorbar(cax=axes([0.93,0.2,0.01,0.65]))
savefig('figures_age/Keff_year.pdf')

    
    
    
    
