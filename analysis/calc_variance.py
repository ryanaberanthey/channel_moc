import channel
from effdiff import effdiff
from pylab import *

base_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/'
c = channel.Channel(base_dir + 'output/reference/ptracers_Keff', base_dir + 'grid')
lc = c.get_layers_computer()

iter0 = 4838400
dT = 2880l

Ntot = 20*12 # 20 years of output
Nt = 24      # resetting every 10 years

q2 = empty(Ntot)
gradq2 = empty(Ntot)

for n in arange(Ntot):
    iter = iter0 + dT*n
    tr = c.rdmds('PTRACER01', iter)
    q2[n] = c.average_vertical(tr**2).mean()
    gradq2[n] = c.average_vertical(
        c.ddx_cgrid_centered(tr)**2 + c.ddy_cgrid_centered(tr)**2
        ).mean()

deltaTime = dT * 900
knum = 0.5*ma.masked_array(-diff(q2)/deltaTime / (0.5*(gradq2[:-1]+gradq2[1:])),zeros(Ntot-1))
knum.mask[::Nt]=True
knum_mean = zeros(Nt)
for n in arange(Nt):
    knum_mean[n] = knum[n::Nt].mean()

t = arange(Ntot-1) / 12.

clf()
plot(t,knum, 'k')
plot(t,50*ones_like(knum), 'k--')
xlabel('Time (years)')
ylabel(r'$\kappa_{num}$ (m$^2$s$^{-1}$)')
title('Numerical Diffusion')
ylim([0,80])
text(6,70, r'mean $\kappa_{num}$ = %3.1f m$^2$s$^{-1}$' % knum.mean())

savefig('figures/knum.pdf')
