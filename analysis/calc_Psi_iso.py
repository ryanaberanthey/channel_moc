from pylab import *
from mdsio import read_mds

data_dir = '/Users/rpa/tmp/channel/'

tau_nb = [7,30,60,120]

iter = 39244800
Lx = 1000*1000.

G = read_mds(data_dir + 'layers_G')

Psi = []

# get the original Psi and interp it
psi_o = np.load('K_orig_psi.npz')
Ny = 400
Ng = len(G)
psi_tmp = zeros((Ng,Ny))
for j in arange(Ny):
    psi_tmp[:,j] = interp(G,psi_o['G'],psi_o['Psi'][:,j])

Psi.append(psi_tmp)

N = len(tau_nb)
for n in (arange(N-1)+1):
    V = transpose(
        read_mds('%s/K_nb%03d/layers_VFlux-tave' % (data_dir,tau_nb[n]), iter),
        (2,1,0))
    Psi.append( -(V.mean(axis=2)).cumsum(axis=0) * Lx )

# find max and min
j = 350
psi_min = zeros(N)
psi_max = zeros(N)
for n in arange(N):
    psi_min[n] = (Psi[n][3:10]).min()
    psi_max[n] = (Psi[n][6:25]).max()
    


