from pylab import *
import channel

base_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/'
c = channel.Channel(base_dir + 'output/reference/ptracers_Keff', base_dir + 'grid')

d = np.load('data/Le2_monthly.npz')

Ng = 42
Ntot = 240
Nt = 24
N = 100 # Keff levels
k0 = 56

yc = c.yc
Y = d['Y']

Le2 = zeros((Nt,c.Nz,N))
Le2_count = zeros_like(Le2)
Le2_g = zeros((Nt,Ng,N))
Le2_g_count = zeros_like(Le2_g)

for n in arange(Ntot):
    ni = n % Nt
    myLe2 = d['Le2'][n]
    Le2[ni] += ma.masked_array(myLe2,isnan(myLe2)).filled(0.)
    Le2_count[ni] += ~isnan(myLe2)
    myLe2g = d['Le2_g']
    Y_g = d['Y_g']
    for k in arange(Ng):
        Le2_gi = interp(Y, Y_g[n,k], myLe2g[n,k], right=nan, left=nan)
        Le2_g[ni,k] += ma.masked_array(Le2_gi,isnan(Le2_gi))
        Le2_g_count[ni,k] += ~isnan(Le2_gi)

Le2 = ma.masked_array(Le2 / Le2_count, Le2_count==0)
Le2_g = ma.masked_array(Le2_g / Le2_g_count, Le2_g_count==0)

# interpolate isopycnal to height
h = c.rdmds('../tave/layers_HV-tave', 5529600, useMask=False).mean(axis=2)
hi = empty((Ng,N))
Le2_gz = empty_like(Le2)
for k in arange(Ng):
    hi[k] = interp(Y,c.yc,h[k])
lc = c.get_layers_computer()
for n in arange(Nt):
    Le2_gz[n] = lc.transform_g_to_z(Le2_g[n],hi)

Le2_g_mean = Le2_g[6:12].mean(axis=0)
Le2_gz_mean = lc.transform_g_to_z(Le2_g_mean, hi)
Le2_gz_mean = ma.masked_equal(Le2_gz_mean,0)
Le2_mean = Le2[6:12].mean(axis=0)

Le2 = ma.masked_invalid(Le2)
Le2_g = ma.masked_invalid(Le2_g)
Le2_gz = ma.masked_invalid(Le2_gz)

np.savez('data/Keff_mean.npz', G=lc.G.squeeze(), Keff_iso=(k0*Le2_g_mean/c.Lx**2).filled(0.),
                                mask=Le2_g_mean.mask,Keff_z=(k0*Le2_mean/c.Lx**2).filled(0.), Y=Y)

Klim = (0,7000)
Klevs = arange(0,7001,500)

close('all')
rc('figure.subplot', left=0.08, right=0.92, bottom=0.2, top=0.85, wspace=0.25)
rc('lines', linewidth=0.5)
rc('font', size=6)
figure(figsize=(6.5,2.1))
for n in arange(0,Nt):
    clf()
    subplot(131)
    contourf(Y/1000.,c.zc, k0 * Le2[n] / c.Lx**2, Klevs, extend='max')
    title(r'$K_{eff}^H$ (m$^2$s$^{-1}$)')
    xlabel('Y (km)'); ylabel('Z (m)')
    subplot(132)
    contourf(Y/1000.,squeeze(lc.G), k0 * Le2_g[n] / c.Lx**2, Klevs, extend='max')
    ylim([0,8])
    title(r'$K_{eff}^{iso}$ (m$^2$s$^{-1}$)')
    xlabel('Y (km)'); ylabel('T ($^\circ$C)')
    subplot(133)
    contourf(Y/1000.,c.zc, k0 * Le2_gz[n] / c.Lx**2, Klevs, extend='max')
    title(r'$K_{eff}^{iso}$ (m$^2$s$^{-1}$)')
    xlabel('Y (km)'); ylabel('Z (m)')
    yticks(arange(-2500,0,500),[])
    colorbar(cax=axes([0.93,0.2,0.01,0.65]))
    savefig('figures/Keff_month%02d.pdf' % n)
    

    
    
    
    
