import channel
from effdiff import effdiff
from pylab import *

base_dir='/Users/rpa/Data/DATASTORE.RPA/projects/channel/V2/'
c = channel.Channel(base_dir + 'output/reference/ptracers_Keff', base_dir + 'grid')
lc = c.get_layers_computer()

# variables needed for Keff calculation
rac = ma.masked_array(c.rac, c.mask[0])
dx = tile(c.dxc,[c.Ny,1])
dy = tile(c.dyc,[c.Nx,1]).T

N = 100 # resolution of Keff calculation
eng = effdiff.EffDiffEngine(rac,dx,dy,N)
Nt = 12

Yk = linspace(0,c.Ly,N)

iter0 = 4838400
dT = 2880l

Ntot = 20*12 # 20 years of output
Nt = 24      # resetting every 10 years

Le2 = zeros((Ntot,c.Nz, N))
Le2_g = zeros((Ntot, lc.Ng, N))
tr_mean = zeros((Ntot,c.Nz, c.Ny))
tr_mean_g = zeros((Ntot,lc.Ng, c.Ny))
# equivalent latitudes
Y = zeros(N)
Y_g = zeros_like(Le2_g)

for n in arange(Ntot):
    ni = n % Nt
    iter = iter0 + dT*n
    tr = c.rdmds('PTRACER01', iter)
    t = c.rdmds('THETA', iter)
    tr_mean[n] = tr.mean(axis=2)
    # flat Le2
    for k in arange(c.Nz):
        r = eng.calc_Le2(tr[k])
        Le2[n,k] = r['Le2']    
    # isopycnal Le2
    tr_t, h = lc.interp_to_g(tr,t)
    tr_mean_g[n] = tr_t.mean(axis=2)
    for kg in arange(lc.Ng):
        rac_g = ma.masked_array(rac, tr_t[kg].mask)
        eng_g = effdiff.EffDiffEngine(rac_g,dx,dy,N)
        print(eng_g.tot_area / eng.tot_area)
        Y_g[n,kg] = (c.Ly-eng_g.A[-1]/c.Lx) + (eng_g.A/c.Lx)
        r = eng_g.calc_Le2(tr_t[kg])
        Le2_g[n,kg] = r['Le2']

Y = eng.A/c.Lx # equivalent lat for horizontal calcs

savez('data/Le2_monthly.npz', tr_mean=tr_mean, tr_mean_g=tr_mean_g,
                              Le2=Le2, Le2_g=Le2_g, Y=Y, Y_g=Y_g)

#tr_mean = tr_mean / Nt
#tr_mean_g = tr_mean_g / Nt
#Le2 = ma.masked_array(Le2, isnan(Le2)) / Nt
#Le2_g = ma.masked_array(Le2_g, isnan(Le2_g)) / Nt

