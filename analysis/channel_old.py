"""Python module for working with my MITgcm channel output"""

from pylab import *
import os.path
from MITgcmutils import mds

class ChannelSetup:
    """Contains a general description of the GCM grid and configuration"""
    
    # global variables
    Nx, Ny, Nz = 0, 0, 0
    Lx, Ly, H = 0, 0, 0
    
    def __init__(self, output_dir, grid_dir=None, datafile=None, eosType='LINEAR',
                 gridType='cartesian', useMNC=False, default_iter=0,
                 f0=-1e-4, beta=1e-11, g=9.8, tAlpha=2e-4):
        
        self.output_dir = output_dir
        if grid_dir==None:
            self.grid_dir = output_dir
        else:
            self.grid_dir = grid_dir
        
        self.default_iter=default_iter
        
        if datafile==None:
            self.datafile = os.path.join(output_dir,'data')
        else:
            self.datafile = datafile
        
        self.load_grid()
        print( '%3d %3d %3d' % (Nx,Ny,Nz) )
        
        self.f0 = f0
        self.beta = beta
        self.g = g
        self.tAlpha = 2e-4
    
    def load_grid(self):
        global Nx, Ny, Nz, Lx, Ly, H
        self.xc = mds.rdmds( os.path.join(self.grid_dir, 'XC') )[0,:]
        self.xg = mds.rdmds( os.path.join(self.grid_dir, 'XG') )[0,:]
        self.yc = mds.rdmds( os.path.join(self.grid_dir, 'YC') )[:,0]
        self.yg = mds.rdmds( os.path.join(self.grid_dir, 'YG') )[:,0]
        self.zc = mds.rdmds( os.path.join(self.grid_dir, 'RC') )[:,0,0]
        self.zf = mds.rdmds( os.path.join(self.grid_dir, 'RF') )[:,0,0]
        
        # for derivatives and integrals
        self.rac = mds.rdmds( os.path.join(self.grid_dir, 'RAC') )
        self.dyc = mds.rdmds( os.path.join(self.grid_dir, 'DYC') )[:,0]
        self.dyg = mds.rdmds( os.path.join(self.grid_dir, 'DYG') )[:,0]
        self.dxc = mds.rdmds( os.path.join(self.grid_dir, 'DXC') )[0,:]
        self.dxg = mds.rdmds( os.path.join(self.grid_dir, 'DXG') )[0,:]
        self.dzc = mds.rdmds( os.path.join(self.grid_dir, 'DRC') )[:,0,0]
        self.dzf = mds.rdmds( os.path.join(self.grid_dir, 'DRF') )[:,0,0]
        
        # masks
        self.hFacC = mds.rdmds( os.path.join(self.grid_dir, 'hFacC') )
        self.hFacS = mds.rdmds( os.path.join(self.grid_dir, 'hFacS') )
        self.hFacW = mds.rdmds( os.path.join(self.grid_dir, 'hFacW') )
        
        Nx, Ny, Nz = len(self.xc), len(self.yc), len(self.zc)
        Lx, Ly, H = self.dxg.sum(), self.dyg.sum(), self.dzf.sum()
        
        self.Nx, self.Ny, self.Nz = Nx, Ny, Nz
        self.Lx, self.Ly, self.H = Lx, Ly, H
    
    def rdmds(self, varname, itrs=None, mask=None):
        """Shortcut to mds.rdmds that includes masking capability"""
        if itrs==None:
            itrs = self.default_iter
        
        out = mds.rdmds( os.path.join(self.output_dir, varname), itrs )
        
        if mask != None:
            out = ma.masked_array(out, mask)
        
        return out
    
    def load_zonal_average_tave(self, iter):
        
        # variables to import (with corresponding mds filenames)
        raw_channel_vars = dict(
            T='Ttave', U='uVeltave', V='vVeltave', W='wVeltave',
            T2='TTtave', U2='UUtave', V2='VVtave',
            UV='UVtave', VT='VTtave', WT='WTtave' )


    
    def ddz_cgrid_centered(self, q):
        """Vertical second-order centered difference on the c grid"""
        
        dzc = tile(self.dzc,(Ny,1)).T
        
        out = zeros(q.shape)
        # second order for interior
        out[1:Nz-1,:] = (q[:Nz-2,:] - q[2:,:]) / (dzc[1:-1,:] + dzc[2:,:])
        
        # first order for the top and bottom
        out[0,:] = (q[0,:] - q[1,:]) / dzc[0,:]
        out[-1,:] = (q[-2,:] - q[-1,:]) / dzc[-1,:]
        
        return out
    
    def ddy_cgrid_centered(self, q):
        """Merdional second-order centered difference on the c grid"""
        
        dyc = tile(self.dyc, (Nz,1))
        
        out = zeros(q.shape)
        out[:,1:Ny-1] = (q[:,2:] - q[:,:Ny-2]) / (dyc[:,1:-1] + dyc[:,2:])
        out[:,0] = (q[:,1] - q[:,0]) / dyc[:,0]
        out[:,-1] = (q[:,-1] - q[:,-2]) / dyc[:,-1]
        
        if isinstance(q, ma.masked_array):
            mask = q.mask
            mask[:,1:Ny-1] = q.mask[:,2:] | q.mask[:,:Ny-2]
            out = ma.masked_array(out, mask)
        
        return out
    
    def cgrid_to_vgrid(self, q, neumann_bc=True):
        """Interpolate c-grid variable (cell center) hortizontally
        to the v-grid (cell southern boundary)"""
        # make sure to mask land properly
        # values inside land are zero, so don't try to interpolate through them

        
        # interpolate
        q_north = q.copy() # the current point
        q_south = q.copy()[:,r_[0,:Ny-1]] # the point below
        # apply masks
        if neumann_bc:
            mask = (self.hFacC==0.)
            q_north[mask] = q_south[mask]
            q_south[mask[:,r_[0,:Ny-1]]] = q_north[mask[:,r_[0,:Ny-1]]]
        
        return 0.5 * (q_north + q_south)
    
    def vgrid_to_cgrid(self, q, neumann_bc=False):
        """Interpolate v-grid variable (cell southern bdry) hortizontally
        to the c-grid (cell center)"""
        # make sure to mask land properly
        # values inside land are zero, so don't try to interpolate through them
        # could consider different boundary conditions

        
        # interpolate
        q_south = q.copy() # the current point
        q_north = q.copy()[:,r_[1:Ny,Ny-1]] # the point north
        # apply masks
        if neumann_bc:
            mask = (self.hFacS==0.)
            q_south[mask] = q_north[mask]
            q_north[mask[:,r_[1:Ny,Ny-1]]] = q_south[mask[:,r_[1:Ny,Ny-1]]]
        
        return 0.5 * (q_north + q_south)
    
    def cgrid_to_wgrid(self, q, neumann_bc=True):
        """Interpolate a c-grid variable (cell center) vertically
        to the f-grid (cell top boundary)"""
        
        q_dn = q.copy() # the current point
        q_up = q.copy()[r_[0,0:Nz-1]] # the point above
        # apply masks
        if neumann_bc:
            mask = (self.hFacC==0.)
            q_dn[mask] = q_up[mask]
            q_up[mask[r_[0,0:Nz-1]]] = q_dn[mask[r_[0,0:Nz-1]]]
        
        return 0.5 * (q_dn + q_up)
    
    def wgrid_to_cgrid(self, q, neumann_bc=False):
        """Interpolate a w-grid variable (cell top) vertically
        down to the c-grid (cell center)"""
        
        q_up = q.copy() # the current point
        q_dn = q.copy()[r_[1:Nz,Nz-1]] # the point below
        # apply masks
        if neumann_bc:
            mask = (self.hFacC==0.)
            q_dn[mask[r_[1:Nz,Nz-1]]] = q_up[mask[r_[1:Nz,Nz-1]]]
        # otherwise the point will be naturally interpolated assuming w=0 at the
        # boundary (doesn't account for partial cells!)
        
        return 0.5 * (q_dn + q_up)

# more complex routines
    def get_zonal_avg(self, field_name, iter=None):
        """Read the field mds file and take a zonal average"""
        return self.rdmds(field_name, iter).mean(axis=2)
    
    def get_N2(self, iter=None):
        b = self.g * self.tAlpha * self.get_zonal_avg('Ttave')
        return self.ddz_cgrid_centered(b)
    
    def get_psi_iso(self, iter=None):
        """Read output from layers package to constructe isopycnal streamfunction."""
        
        V = self.get_zonal_avg('layers_VFlux-tave', iter)
        return -V.cumsum(axis=0)*self.Lx
    
    def get_psi_iso_z(self, iter=None):
        """Put the output from psi_iso into Z coordinates."""
        
        psi_iso = self.get_psi_iso(iter)
        # figure out the depth of each layer
        h = self.get_zonal_avg('layers_HV-tave', iter)
        # psi_iso is defined at the *bottom* of each layer,
        # therefore we want the depth at the bottom of the layer
        z = cumsum(h, axis=0) - self.H
        
        # interpolate to center z points
        psi_iso_z = zeros((Nz,Ny))
        for j in arange(Ny):
            psi_iso_z[:,j] = interp(self.zc, z[:,j], psi_iso[:,j])

        return psi_iso_z
    
    # use c instead of self, shorter to write
    def get_qgpv_grad(c, iter=None):
        """Calculate QGPV gradient from standard output fields"""
        T = c.get_zonal_avg('Ttave')
        # isopycnal slope
        s =  - c.ddy_cgrid_centered(T) / c.ddz_cgrid_centered(T)
        return c.beta - c.f0 * c.ddz_cgrid_centered(s)




